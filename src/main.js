

class Vector2{
    x = 0;
    y = 0;
    constructor(x,y){
        this.x = x;
        this.y = y;
    }

    sqDistance(point){
        return  Math.pow(point.x - this.x,2) + Math.pow(point.y - this.y,2);
    }

    distance(point){
        return  Math.sqrt(this.sqDistance(point));
    }

    minus(point){
        return new Vector2 (this.x - point.x,this.y - point.y);
    }

    normalize(){
        let distance = this.distance(new Vector2(0,0));
        return new Vector2(this.x/distance, this.y/distance);
    }

    equals(point){
        return this.x == point.x && this.y == point.y;
    }
}

class SnogardFunctions{
    print(text){
        console.log(text);
    }

    async getTokensInSight(placeable){
        let tokensInCanvas = games.canvas.tokens.placeables;
        await placeable.control({});
        await this.sleep(2000); //TODO: find a way to  wait for animation finish
        
        let result = [];
        for(let i=0; i<tokensInCanvas.length; i++){
            let tk = tokensInCanvas[i];
            if (tk.visible && (tk.visible && (!tk.document.hidden || filter.seeInvisibility) && placeable.document._id != tk.document._id)){
                result.push(tk);
            }
        }

        return result;
    }

    // filters are or
    // returns token info and distance measrured in squares
    async getNearestTokenInSight(placeable,filter){
        //disposition
        //names
        if(filter.ids && filter.ids.length==0){filter.ids = undefined;}
        //let allTokens = game.scenes.current.tokens._source;
        let allTokens = game.canvas.tokens.placeables
        let filteredTokens = game.canvas.tokens.placeables
        if(filter){
            filteredTokens = [];
            for(let i=0; i<allTokens.length; i++){
                let tk = allTokens[i];
                 if(!filter.ids && tk.document.disposition == filter.disposition){
                     filteredTokens.push(tk);
                 }
                
                if(filter.ids && filter.ids.includes(tk.actorId)){
                    filteredTokens.push(tk);
                }
            }
        }

        let nearest;
        let smallestDistance = 100000000;
        
        await placeable.control({});
        await this.sleep(2000);//TODO: find a way to  wait for animation finish

        for(let i=0; i<filteredTokens.length; i++){
            let tk = filteredTokens[i];

            if (tk.visible && (!tk.document.hidden || filter.seeInvisibility) && placeable.document._id != tk.document._id){
                let p1 = new Vector2(placeable.document.x,placeable.document.y);
                let p2 = new Vector2(tk.document.x,tk.document.y);
                let distance = p1.distance(p2);

                if(distance<smallestDistance){
                    console.log(tk);
                    console.log(""+tk.visible);
                    nearest = tk;
                    smallestDistance = distance;
                }
            }
        }

        smallestDistance = smallestDistance / game.scenes.current.dimensions.size;

        if(nearest){
            return {
                token: nearest.document,
                distance: smallestDistance,
            };
        }

        return;
    }

    getTokenByActorId(id){
        //do some kind of update
        let tokens = game.scenes.current.tokens._source;
        for (let i=0; i< tokens.length; i++){
            if (tokens[i].actorId == id){
                return tokens[i];
            }
        }
        return undefined;
    }

    getTokenById(id){
        let tokens = game.scenes.current.tokens._source;
        for (let i=0; i< tokens.length; i++){
            if (tokens[i]._id == id){
                return tokens[i];
            }
        }
        return undefined;
    }

    getPlaceableByActorId(id){
        let placeables = game.canvas.tokens.placeables;
        // console.log("pending operation:");
        // console.log(game.canvas.pendingOperations);

        for (let i=0; i< placeables.length; i++){
            if (placeables[i].document.actorId == id){
                console.log(placeables[i]);
                return placeables[i];
            }
        }

        return;
    }

    createDialog(title, content){
        let result = {};
        let dialogPromise = new Promise((resolve,reject) =>
        {
            let dialog = new Dialog({
                title: title,
                content: content,
                buttons: {
                    ok: {
                        label: "Ok",
                        callback: (html) => {resolve();},
                    }
                },
                close: () => {resolve();},
            });
            result.dialog = dialog;
            dialog.render(true);
        });
        result.promise = dialogPromise;

        return result;
    }

    getActorById(id){
        return game.actors.get(id);
    }

    #canSee(token,target){
        return true;
    }

    #getSceneScale(){
        return;
    }
    async sleep(ms){
        await new Promise(r => setTimeout(r, ms));
    }
}


export class Drone{
    #sf = {};
    #journalID="";
    #droneInfo = {};
    #turnInfo={};
    #log = [];
    #saveData = {};

    constructor(args){
        this.#sf = new SnogardFunctions();      
        this.#journalID = args.journalID;
        this.#droneInfo = this.#getDroneInfo();
        this.#turnInfo.usedActions = [];
        this.#turnInfo.isMoving;

        //TODO: add loaded bolts to turn info.
    }

    // requested by players


    // spells
    async getSpells(){
        let fname="getSpells";
        if(await this.#isUnlocked(fname)){
            return;
        }
        return this.#functionLockedError(fname);
    }
    
    async castSpell(spellInfo,targets){
        let fname="useSpell";
        if(await this.#isUnlocked(fname)){
            let info = await this.#sf.createDialog("cast spell", "<pre>"+JSON.stringify({spellInfo : spellInfo, targets: targets}, null,2)+"</pre>");
            await info.promise;
            return;
        }
        return this.#functionLockedError(fname);
    }

    async getCurrentManaPoints(){
        return 0; //TODO: implement this
    }

    // shield bash
    async shieldBash(attackData,targets){
        return undefined; //TODO implement this?
    }

    // crossbow
    async getBolts(){
        let fname="getBolts";
        if(await this.#isUnlocked(fname)){
            let quiverContents = this.#getItemById(this.#droneInfo.quiverId).items.contents;
            let inventoryContents = this.#getActor().items.contents;
            let result = {};
            result.quiver = [];
            result.quiverBolts = 0;
            result.load = [];
            result.loadedBolts = 0;
            //result.loaded = [];
            for(let i=0; i<quiverContents.length; i++){
                let item = quiverContents[i];
                let ammo = {};
                if ( item.system.extraType == "bolt" && item.system.quantity != 0)
                {
                    ammo.id = item._id;
                    ammo.name = item.name;
                    ammo.quantity = item.system.quantity;
                    ammo.loaded = false;

                    result.quiverBolts += ammo.quantity;
                    result.quiver.push(ammo);
                }
                else{
                    return this.#createAndLogError("ERROR: '"+ammo.name+"'"+" is not an bolt, please remove");
                }
            }

            for(let i=0; i<inventoryContents.length;i++){
                let item = inventoryContents[i];
                let ammo = {};

                if(item.system.extraType == "bolt" && item.system.quantity != 0){
                    ammo.id = item._id;
                    ammo.name = item.name;
                    ammo.quantity = item.system.quantity;
                    ammo.loaded = true;

                    result.loadedBolts += ammo.quantity;
                    result.load.push(ammo);
                }
            }

            result.maxLoad = 1;
            if(this.#isUnlocked("doubleCrossbow")){
                result.maxLoad = 2;
            }
            if(this.#isUnlocked("repeatingCrossbow")){
                result.maxLoad = 5;
            }

            if (result.quiverBolts > this.#droneInfo.quiverSpace){
                return this.#createAndLogError("ERROR: Too much ammo inside quiver, cannot operate crossobow functionality");
            }

            if(result.loadedBolts > result.maxLoad){
                return this.#createAndLogError("ERROR: Too much ammo is loaded, cannot operate crossbow functionality");
            }

            return result;
        }
        return this.#functionLockedError(fname);
    }

    // bolts = [{name: string, quantity: int}]
    async loadBolts(bolts=[]){
        let fname="loadBolts";
        let requiredAction="full-round";
    
        if(await this.#isUnlocked(fname)){
            if (this.#isUnlocked("featRapidReload")){
                requiredAction="standard";
            }
            if (this.#isUnlocked("featCrossbowMastery")){
                requiredAction="free";
            }

            let boltInfo = this.getBolts();
            if (!this.#canPerformAction(requiredAction)){
                return this.#createAndLogError("ERROR: cannot perform "+requiredAction+" to load bolts");
            }
            if(boltInfo.error){
                return this.#createAndLogError(boltInfo.error);
            }
            if(boltInfo.loadedBolts == boltInfo.maxLoad){
                return this.#createAndLogError("ERROR: maximum bolts already loaded");
            }


            // let info = await this.#sf.createDialog("load bolts", "<pre>"+JSON.stringify(bolts, null,2)+"</pre>");
            // await info.promise;
            
            let quiverContents = this.#getItemById(this.#droneInfo.quiverId).items.contents;
            let inventoryContents = this.#getActor().items.contents;

            let loadedBolts = boltInfo.loadedBolts;
            let maxLoad = boltInfo.maxLoad;

    
            for(let i = 0; i < bolts.length; i++){
                let bolt = bolts[i];
                let quiverBoltItem = undefined;
                let invetoryBoltItem = undefined;

                for(let j = 0; j < quiverContents.length; j++){
                    let quiverBolt = quiverContents[j];
                    if(item.system.extraType == "bolt" && quiverBolt.name == bolt.name){
                        quiverBoltItem = quiverBolt;
                        break;
                    }
                }

                for(let j = 0; j < inventoryContents.length; j++){
                    let invBolt = inventoryContents[j];
                    if(item.system.extraType == "bolt" && invBolt.name == bolt.name){
                        invetoryBoltItem = invBolt;
                        break;
                    }
                }

                if(quiverBoltItem && invetoryBoltItem){
                    let toLoadQuantity = Math.max(Math.min( bolt.quantity, quiverBoltItem.system.quantity ,maxLoad-loadedBolts),0);
                    if(toLoadQuantity == 0){continue;}
                    await quiverBoltItem.update({
                        system:{
                            quantity: quiverBoltItem.system.quantity - toLoadQuantity,
                        }
                    });
                    await invetoryBoltItem.update({
                        system:{
                            quantity: invetoryBoltItem.system.quantity + toLoadQuantity,
                        }
                    });
                    
                }
                else{
                    return this.#createAndLogError("ERROR: "+bolt.name+" is missing from the quiver or from the inventory")
                }

                if(loadedBolts == bolt.maxLoad){break;}

            }

            this.#performAction(requiredAction);
            return true;
        }
        return this.#functionLockedError(fname);
    }

    async getCrossbowInfo(){
        let fname="getCrossbowInfo";
        if(await this.#isUnlocked(fname)){
            let action = this.#getItemById(this.#droneInfo.crossbowAttackId).firstAction.data;
            let actor = this.#getActor();
            let info = [];
    
            info.rangeIncrement = action.range.value / game.scenes.current.dimensions.size;
            info.damageFormula = action.parts[0].formula;
            info.babAttacks = 1 + Math.floor((actor.system.attributes.bab.total - 1)/5);

            //TODO add loaded bolts;

            return info;
        }
        return this.#functionLockedError(fname);
    }

    // attackData.rapidShot: bool
    // attackData.fullAttack: bool
    //TODO implement default bolt
    // attackData.defaultBolt: string of bolt id (used in case loaded bolts finish)
    // targets.tokens: [{id,quantity},{...}] of token ids
    // targets.positions: [{x,y},{...}] list of canvas positions
    async crossbowAttack(attackData,targets,gmMessage){
        let fname="crossbowAttack";
        let requiredAction = "standard";
        if(await this.#isUnlocked(fname)){
            attackData.rapidShot = attackData.rapidShot && this.#isUnlocked("featRapidShot");
            attackData.freeReload = this.#isUnlocked("featCrossbowMastery");
            requiredAction = attackData.rapidShot || attackData.fullAttack ? "full-round" : "standard";

            if(!this.#canPerformAction(requiredAction)){
                return this.#createAndLogError("ERROR: cannot perform "+requiredAction+" action");
            }
     
            let targetIds = [];
            let targetQts = [];
            if(targets.tokens && targets.tokens.length > 0){
                //TODO: check how many attacks can be done and reduce targets to max number of attacks
                for(let i=0; i< targets.tokens.length; i++){
                    targetIds.push(targets.tokens[i].id);
                    targetQts.push(targets.tokens[i].quantity);
                }
                await game.user.updateTokenTargets(targetIds);
            }
            else{
                return this.#createAndLogError("ERROR: no target selected for crossbow attack");
            }

            let crossbowAttackItem = this.#getItemById(this.#droneInfo.crossbowAttackId);

            // create info for gm
            let gmInfo = {
                message: gmMessage,
                targets: [],
            };

            gmInfo.defaultBolt = attackData.defaultBolt;

            if(requiredAction == "full-round"){
                gmInfo.attackType="full-round"
                if(attackData.rapidShot){
                    gmInfo.rapidShot = true;
                }
            }
            else{
                gmInfo.attackType="single";
            }
        
            for(let i=0; i< targetIds.length; i++){
                let token = this.#sf.getTokenById(targetIds[i]);
                let info = {}

                info.id = token._id;
                info.name = token.name;
                info.attacks = targetQts[i];
                info.image = "";

                gmInfo.targets.push(info);
                this.#log.push({text:"Performed "+targetQts[i]+" against "+token._id});
            }

            //add ammunition to gm info

            //------------
            
            // info for the gm
            let infoDialogPromise = new Promise((resolve,reject) =>
            {
                let dialog = new Dialog({
                    title: "Drone: Make an attack",
                    content: "<pre>"+JSON.stringify(gmInfo, null,2)+"</pre>",
                    buttons: {
                        ok: {
                            label: "Ok",
                            callback: (html) => {resolve();},
                        }
                    },
                    close: () => {resolve();},
                }).render(true);
            });

            let useData = await crossbowAttackItem.use();
            await infoDialogPromise;

            console.log("use data:");
            console.log(useData);

            this.#performAction(requiredAction);

            return true;
        }
        return this.#functionLockedError(fname);

    }

    // wands
    async getWands(){
        let fname="getWands";
        if(await this.#isUnlocked(fname)){
            return;
        }
        return this.#functionLockedError(fname);
    }

    async useWand(wandInfo, targets){
        let fname="useWand";
        if(await this.#isUnlocked(fname)){
            let info = await this.#sf.createDialog("use wand", "<pre>"+JSON.stringify({wandIndo: wandInfo, targets: targets}, null,2)+"</pre>");
            await info.promise;
            return;
        }
        return this.#functionLockedError(fname);
    }
    
    // scrolls
    async getScrolls(){
        let fname="getScrolls";
        if(await this.#isUnlocked(fname)){
            return;
        }
        return this.#functionLockedError(fname);
    }

    async useScroll(scrollInfo,targets){
        let fname="useScroll";
        if(await this.#isUnlocked(fname)){
            let info = await this.#sf.createDialog("use scroll", "<pre>"+JSON.stringify({scrollInfo : scrollInfo, targets: targets}, null,2)+"</pre>");
            await info.promise;
            return;
        }
        return this.#functionLockedError(fname);
    }

    // potions
    async getPotions(){
        let fname="getPotions";
        if(await this.#isUnlocked(fname)){
            return;
        }
        return this.#functionLockedError(fname);
    }

    async sprayPotion(potionInfo,direction){
        let fname="sprayPotion";
        if(await this.#isUnlocked(fname)){
            let info = await this.#sf.createDialog("spray potion", "<pre>"+JSON.stringify({potionInfo : potionInfo, direction: direction}, null,2)+"</pre>");
            await info.promise;
            return;
        }
        return this.#functionLockedError(fname);
    }

    // base
    async getEntitiesInSight()
    {
        let fname="getEntitiesInSight"
        if(await this.#isUnlocked(fname)){
            let placeable = this.#getPlaceable();
            let tokens = await this.#sf.getTokensInSight(placeable);
            let result = [];
            if(tokens.length > 0){
                for(let i=0; i<tokens.length; i++){
                    let tk = tokens[i];

                    let attributes = tk.actor.system.attributes;

                    let start = new Vector2(placeable.document.x,placeable.document.y);
                    let end = new Vector2(tk.document.x,tk.document.y);
                    let distance = start.distance(end);
                    let direction = end.minus(start).normalize();

                    let element = {};
                    element.actorId = tk.document.actorId;
                    element.tokenId = tk.document._id;
                    if(this.#isUnlocked("scannerDistance")){
                        element.distance = distance;
                    }
                    if(this.#isUnlocked("scannerDirection")){
                        element.direction = direction;
                    }
                    if(this.#isUnlocked("scannerPosition")){
                        element.position = end;
                    }
                    if(this.#isUnlocked("scannerHealth")){
                        let vigor = attributes.vigor;
                        let wounds = attributes.wounds;
                        element.health = (vigor.value + wounds.value) / (vigor.max + wounds.max);
                    }
                    if(this.#isUnlocked("scannerAC")){
                        let ac = attributes.ac.normal;
                        let rangedAttack = attributes.attack.ranged;
                        let percentage = Math.max(20 - ac + rangedAttack + 1,1) * 5 /100
                        element.toHitAC = percentage;
                    }
                    if(this.#isUnlocked("scannerTouchAC")){
                        let touchAC = attributes.ac.touch;
                        let rangedAttack = attributes.attack.ranged;
                        let percentage = Math.max(20 - touchAC + rangedAttack + 1,1) * 5 /100
                        element.toHitTouchAC = percentage;
                    }
                    if(this.#isUnlocked("scannerSpellResist")){
                        let sr = attributes.sr.total;
                        let cl = attributes.spells.spellbooks.primary.cl.total;
                        let percentage  = Math.max(20 - sr + cl + 1,1) * 5 /100
                        element.toHitSR = percentage;
                    }

                    result.push(element);
                }
            }
            
            this.#log.push("Found "+result.length+" Entities");
            return result;

        }
        return this.#functionLockedError(fname);
    }

    // initial
    async freeStep(dx,dy)
    {
        if(this.#canPerformAction("move") != this.#turnInfo.isMoving){
            return this.#createAndLogError("ERROR: move action already used to move");
        }

        this.#turnInfo.isMoving=true;

        dx = Math.round(dx);
        dy = Math.round(dy);

        if(dx!=0){ dx = dx/dx*Math.sign(dx); }
        if(dy!=0){ dy = dy/dy*Math.sign(dy); }

        this.#log.push({text: "Next movement is a 5 foot step"});

        return await this.#move(dx,dy,"fly"); 
    }

    async fly(dx,dy){
        if(this.#canPerformAction("move") != this.#turnInfo.isMoving){
            return this.#createAndLogError("ERROR: move action already used");
        }

        this.#performAction("move");
        this.#turnInfo.isMoving = true;

        dx = Math.round(dx);
        dy = Math.round(dy);

        return await this.#move(dx,dy,"fly");
    }
    
    async #move(dx,dy,movementType){

        let placeable = this.#getPlaceable();
        let id = placeable.id;
        let startPosition = this.#getCurrentPosition(placeable);
        let maxDistance = this.#getSpeed(placeable.actor, movementType);
        let moveDistance = new Vector2(startPosition.x+dx,startPosition.y+dy).distance(startPosition);

        if(!this.#turnInfo.movement){
            let movement = {}
            movement.startPosition = startPosition;
            movement.methodOfTravel = movementType;
            movement.distance = 0;
            this.#turnInfo.movement = movement;
        }

        if(this.#turnInfo.movement.distance +  moveDistance > maxDistance){
            this.#log.push({text: "Movement failed: max distance reached"});
            return {
                startPosition: startPosition,
                endPosition: startPosition,
                moveDistance: moveDistance,
                moveDistanceLeft: maxDistance - this.#turnInfo.movement.distance,
                error: "Movement outside of max flight distance",
            };
        }

        await game.canvas.tokens.moveMany({dx:dx, dy:dy, ids:[id]});
        await this.#sf.sleep(500*Math.ceil(moveDistance)); //TODO: find a way to wait for animation finish

        let endPosition = this.#getCurrentPosition(placeable);
        if(endPosition.equals(startPosition)){
            this.#log.push({text: "Movement failed: obstruction detected"});
            return {
                startPosition: startPosition,
                endPosition: endPosition,
                moveDistance: moveDistance,
                moveDistanceLeft: maxDistance - this.#turnInfo.movement.distance,
                error: "Movement blocked due to obstuction",
            }
        }
        else{
            this.#turnInfo.movement.distance += moveDistance;
            this.#log.push({text: "Moved from "+startPosition.x+","+startPosition.y+" to "+endPosition.x+","+endPosition.y});
            return {
                startPosition: startPosition,
                endPosition: endPosition,
                moveDistance: moveDistance,
                moveDistanceLeft: maxDistance - this.#turnInfo.movement.distance,
            };
        }
    }

    async printLog(){
        let fname="printLog";
        if(await this.#isUnlocked(fname)){
            let msg="";
            for(let i=0; i<this.#log.length; i++){
                msg += this.#log[i].text+"\n";
            }
            return true;
        }
        else{
            return this.#functionLockedError(fname);
        }
    }

    async printLogToChat(){
        let fname="printLogToChat"
        if(await this.#isUnlocked(fname)){
            let msg="";
            let actor = this.#sf.getActorById(this.#droneInfo.actorId);

            for(let i=0; i<this.#log.length; i++){
                msg += this.#log[i].text+"<br>";
            }
            //TODO add better speaker detection
            await ChatMessage.create({
                speker: ChatMessage.getSpeaker({actor: actor}),
                content: msg,
            });

            return true;
        }
        else{
            return this.#functionLockedError(fname);
        }

    }

    async isWithinLineCapacity(){
        let linesMul = 0; //TODO: get lines per point of wisdom
        let wisdom = 0; //TODO: get wisdom score
        
        return (await this.getLinesOfCodeCount() <= (wisdom * linesMul))
    }

    async getLinesOfCodeCount(){
        return await this.getLinesOfCodeStats().count;
    }

    async getLinesOfCodeStats(){
        let macro = game.macros.get(this.#droneInfo.turnMacroId);
        let sourceCode = macro.command;
        let lines = sourceCode.split("\n");
        
        let result = {};
        result.count = 0;
        result.skipped = 0;
        lines.forEach(line => {
            let processedLine = line.replaceAll("\t","");
            if (
                processedLine == ""
                || processedLine == "{"
                || processedLine == "}"
                || processedLine.includes("function")
                || processedLine.includes("console.log")
                || processedLine.includes("drone.printLog")
                || processedLine.includes("drone.printLogToChat")
                || processedLine.includes("droneFactory.createNewDrone")
                || processedLine.includes("let allies")
                || processedLine == "Execute()"
            )
            {
                result.skipped++;
            }
            else{
                result.count++;
            }
        });

        return result;
    }
    // ------------

    #getPlaceable(){
        let id = this.#droneInfo.actorId;
        return this.#sf.getPlaceableByActorId(id);
    }

    #getActor(){
        let id = this.#droneInfo.actorId
        return this.#sf.getActorById(id);
    }

    // #getItems(){
    //     return this.#getActor().items._source
    // }

    #getItemById(id)
    {
        let allItems = this.#getActor().allItems;
        for(let i=0; i<allItems.length; i++){
            if (allItems[i]._id == id){
                return allItems[i];
            }
        }
        return undefined;
    }

    #getToken(){
        let id = this.#droneInfo.actorId;
        return this.#sf.getTokenByActorId(id);
    }

    #getDroneInfo(){
        let result = {};
        
        let journal = game.journal.get(this.#journalID)
        if(journal){
            for(let j=0; j<journal.pages.size; j++){
                let page = journal.pages._source[j];
                let content = page.text.content;
                switch(page.name){
                    case "data":
                        let info = content.replaceAll("<p>","").split("</p>");
                        for(let k=0;k<info.length; k++){
                            let split = info[k].split("::");
                            result[split[0]]=split[1];
                        }
                        break;
                    case "unlocks":
                        result.unlocks = content.replaceAll("<p>","").split("</p>");
                        break;
                }
            }
        }

        if(!result.scanRange){
            result.scanRange = 18
        }
        console.log(result);
        return result;
    }

    #functionLockedError(fname){
        let text = "Function '"+fname+"' is locked";
        return this.#createAndLogError(text);
    }

    #tokenNotFoundError(){
        return {error: "Token not found"};
    }

    async #isUnlocked(fname){
        return this.#droneInfo.unlocks.includes(fname);
    }

    #getSpeed(actor,speedName){
        return actor.system.attributes.speed[speedName].total / game.scenes.current.dimensions.distance;
    }

    #getCurrentPosition(placeable){
        let canvasSize = game.scenes.current.dimensions.size;
        return new Vector2(placeable.x / canvasSize, placeable.y / canvasSize);
    }

    #createAndLogError(text){
        this.#log.push({text: text});
        return {error: text};
    }

    #canPerformAction(action){
        switch(action){
            case "standard":
            case "move":
            case "swift":
                return !this.#turnInfo.usedActions.includes(action);
            case "full-round":
                return !this.#turnInfo.usedActions.includes("standard") && !this.#turnInfo.usedActions.includes("move");
            case "step": //TODO rethink
                return !this.#turnInfo.isMoving;
            case "free":
                return true;
        }
    }

    #performAction(action){
        switch(action){
            case "standard":
            case "move":
            case "swift":
            case "immediate":
                this.#turnInfo.usedActions.push(action);
                break;
            case "full-round":
                this.#turnInfo.usedActions.push("standard");
                this.#turnInfo.usedActions.push("move");
                break;
            case "step": //TODO rethink
                this.#turnInfo.isMoving=true;
                break;
            case "free":
                break;
        }
    }

}

export class DroneFactory{
    async createNewDrone(args){
        return new Drone(args);
    }
}

globalThis.droneFactory = new DroneFactory();
globalThis.sf = new SnogardFunctions();
